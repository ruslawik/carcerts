<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GotCertLog extends Model
{
    use HasFactory;

    protected $table = "got_certs_logs";
}
