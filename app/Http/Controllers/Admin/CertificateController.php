<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCertificateRequest;
use App\Http\Requests\StoreCertificateRequest;
use App\Http\Requests\UpdateCertificateRequest;
use App\Models\Certificate;
use App\Models\Page;
use App\Models\Item;
use App\Models\Person;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Spatie\Browsershot\Browsershot;
use Carbon\Carbon;
use App\Models\GotCertLog;

class CertificateController extends Controller
{

    public function downloadMappCert($email, $lang = 'ru'){
        $host_api_url = env("MAPP_PASSED_API_URL", "https://test.careervision.kz");

        $user_info = json_decode(file_get_contents($host_api_url."/is-mapp-passed/".$email));

        $cert_id = 3;
        if($lang == 'kz'){
            $cert_id = 4;
        }

        if($user_info->status == "yes"){
            $ar['pages'] = Page::where('cert_id', $cert_id)->orderBy('number')->get();
            $ar['name'] = $user_info->name;
            $ar['cert_num'] = "C2021".$user_info->user_id;
            $ar['cert_id'] = $cert_id;
            $ar['mapp_passed_date'] = Carbon::parse($user_info->mapp_finished_date)->format('d.m.Y H:i:s');

            $html = view('get_cert', $ar);
            $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

            $get_orientation = Page::where('cert_id', $cert_id)->limit(1)->get();
            $orientation = $get_orientation[0]->orientation;

            if($orientation == "land"){
                Browsershot::html($html)->showBackground()->windowSize(595, 842)->paperSize(297, 210)->save($user_info->name.'.pdf');
            }
            if($orientation == "vert"){
                Browsershot::html($html)->showBackground()->windowSize(842, 595)->paperSize(210, 297)->save($user_info->name.'.pdf');
            }

            $log = new GotCertLog();
            $log->email = $email;
            $log->name  = $user_info->name;
            $log->cert_num  = $ar['cert_num'];
            $log->bergen_date = Carbon::now()->timezone('Asia/Almaty');
            $log->cert_id = $cert_id;
            $log->save();

            return response()->download(public_path($user_info->name.'.pdf'))->deleteFileAfterSend(true);
        }else{
            return view("cert_not_found");
        }
    }

    public function check_by_qr ($cert_id, $user_id){

        $result = Person::where('certificate_id', $cert_id)->where('id', $user_id)->count();

        if($result > 0){
            $person_data = Person::where('certificate_id', $cert_id)->where('id', $user_id)->get();
            $ar['message'] = "Сертификат закреплен за ".$person_data[0]->name." (".$person_data[0]->email.")";
            return view('check_by_qr', $ar);
        }else{
            $ar['message'] = "Такой сертификат не найден.";
            return view('check_by_qr', $ar);
        }

    }

    public function check_cert_page ($cert_id){

        $person = Person::first();

        $ar['pages'] = Page::where('cert_id', $cert_id)->orderBy('number')->get();
        $ar['name'] = $person->name;
        $ar['cert_num'] = "C100".$person->id;
        $ar['cert_id'] = $cert_id;
        $ar['user_id'] = $person->id;
        $ar['mapp_passed_date'] = "12-02-2021 13:45";

        return view('get_cert', $ar);
    }

    public function cert_get_page ($cert_id){

        $ar['cert_id'] = $cert_id;
        $cert_data = Certificate::where('id', $cert_id)->get();
        $ar['cert_name'] = $cert_data[0]->name;

        return view("form", $ar);
    }

    public function download (Request $r){

        $cert_id = $r->input('cert_id');
        $email = $r->input('email');

        $check_query = Person::where('email', $email)->where('certificate_id', $cert_id)->count();

        if($check_query == 0){
            return back()->with('message', 'Такой email не связан ни с одним сертификатом.');
        }

        $person_data = Person::where('email', $email)->where('certificate_id', $cert_id)->get();

        $ar['pages'] = Page::where('cert_id', $cert_id)->orderBy('number')->get();
        $ar['name'] = $person_data[0]->name;
        $ar['cert_num'] = "C100".$person_data[0]->id;
        $ar['cert_id'] = $cert_id;
        $ar['user_id'] = $person_data[0]->id;

        $html = view('get_cert', $ar);
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

        $get_orientation = Page::where('cert_id', $cert_id)->limit(1)->get();
        $orientation = $get_orientation[0]->orientation;

        if($orientation == "land"){
            Browsershot::html($html)->showBackground()->windowSize(595, 842)->paperSize(297, 210)->save($person_data[0]->name.'.pdf');
        }
        if($orientation == "vert"){
            Browsershot::html($html)->showBackground()->windowSize(842, 595)->paperSize(210, 297)->save($person_data[0]->name.'.pdf');
        }

        Person::where('email', $email)->where('certificate_id', $cert_id)
                ->update([
                    "cert_num" => $ar['cert_num'],
                    "bergen_date" => Carbon::now()->timezone('Asia/Almaty'),
                ]);

        return response()->download(public_path($person_data[0]->name.'.pdf'))->deleteFileAfterSend(true);
    }

    public function index()
    {
        abort_if(Gate::denies('certificate_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $certificates = Certificate::all();

        return view('admin.certificates.index', compact('certificates'));
    }

    public function create()
    {
        abort_if(Gate::denies('certificate_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.certificates.create');
    }

    public function store(StoreCertificateRequest $request)
    {
        $certificate = Certificate::create($request->all());

        return redirect()->route('admin.certificates.index');
    }

    public function edit(Certificate $certificate)
    {
        abort_if(Gate::denies('certificate_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.certificates.edit', compact('certificate'));
    }

    public function update(UpdateCertificateRequest $request, Certificate $certificate)
    {
        $certificate->update($request->all());

        return redirect()->route('admin.certificates.index');
    }

    public function show(Certificate $certificate)
    {
        abort_if(Gate::denies('certificate_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $certificate->load('certificatePeople', 'certPages');

        return view('admin.certificates.show', compact('certificate'));
    }

    public function destroy(Certificate $certificate)
    {
        abort_if(Gate::denies('certificate_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $certificate->delete();

        return back();
    }

    public function massDestroy(MassDestroyCertificateRequest $request)
    {
        Certificate::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
