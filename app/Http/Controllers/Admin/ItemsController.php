<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyItemRequest;
use App\Http\Requests\StoreItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Models\Certificate;
use App\Models\Item;
use App\Models\Page;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ItemsController extends Controller
{   

    public function getSelectPagesAjax (Request $r){

        $cert_id = $r->input('certid');

        $pages = Page::where('cert_id', $cert_id)->get();

        $json = array();
        $json['items'] = array();
        $i=0;
        foreach ($pages as $page) {
            $json['items'][$i]['id'] = $page->id;
            $json['items'][$i]['text'] = $page->number;
            $i++;
        }

        return response()->json($json);
    }

    public function index()
    {
        abort_if(Gate::denies('item_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $items = Item::with(['cert', 'page'])->get();

        $certificates = Certificate::get();

        $pages = Page::get();

        return view('admin.items.index', compact('items', 'certificates', 'pages'));
    }

    public function create()
    {

        abort_if(Gate::denies('item_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $certs = Certificate::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $pages = Page::pluck('number', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.items.create', compact('certs', 'pages'));
    }

    public function store(StoreItemRequest $request)
    {
        $item = Item::create($request->all());

        return redirect()->route('admin.items.index');
    }

    public function edit(Item $item)
    {
        abort_if(Gate::denies('item_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $certs = Certificate::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $pages = Page::pluck('number', 'id')->prepend(trans('global.pleaseSelect'), '');

        $item->load('cert', 'page');

        return view('admin.items.edit', compact('certs', 'pages', 'item'));
    }

    public function update(UpdateItemRequest $request, Item $item)
    {
        $item->update($request->all());

        return back();
    }

    public function show(Item $item)
    {
        abort_if(Gate::denies('item_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $item->load('cert', 'page');

        return view('admin.items.show', compact('item'));
    }

    public function destroy(Item $item)
    {
        abort_if(Gate::denies('item_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $item->delete();

        return back();
    }

    public function massDestroy(MassDestroyItemRequest $request)
    {
        Item::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
