<?php

namespace App\Http\Requests;

use App\Models\Item;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreItemRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('item_create');
    }

    public function rules()
    {
        return [
            'cert_id' => [
                'required',
                'integer',
            ],
            'page_id' => [
                'required',
                'integer',
            ],
            'type' => [
                'required',
            ],
            'x_axis' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'y_axis' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
