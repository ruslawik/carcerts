<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->integer('x_axis')->nullable();
            $table->integer('y_axis')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
