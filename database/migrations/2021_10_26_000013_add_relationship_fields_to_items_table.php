<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToItemsTable extends Migration
{
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->unsignedBigInteger('cert_id');
            $table->foreign('cert_id', 'cert_fk_5200653')->references('id')->on('certificates');
            $table->unsignedBigInteger('page_id');
            $table->foreign('page_id', 'page_fk_5200654')->references('id')->on('pages');
        });
    }
}
