<!DOCTYPE html>
<html lang="en">
<head>
	<title>Получить сертификат от Caravan of knowledge</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="https://static.tildacdn.com/tild3461-3762-4133-a231-646630366132/favicon_1.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/util.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100" style="background-image: url('/images/bg-01.jpg');">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" action="/download" method="POST">
				<span class="contact100-form-title">
					<center><img src="/images/caravan_logo.png" width="270"></center>
				</span>
				<input type="hidden" name="cert_id" value="{{$cert_id}}">
				<div class="wrap-input100">
					@csrf
					<center><h3>Выдача сертификатов "{{$cert_name}}"</h3></center>
					<br>
					<center><span class="label-input100">Введите Ваш e-mail:</span></center>
					<input style="text-align:center !important;" class="input100" type="text" name="email" placeholder="email@gmail.com">
					@if (\Session::has('message'))
					    <div class="alert alert-success">
					        <ul>
					        	<br>
					            <center><li>{!! \Session::get('message') !!}</li></center>
					        </ul>
					    </div>
					@endif
				</div>
				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn">
							Получить сертификат!
						</button>
					</div>
				</div>
			</form>
		</div>

		<span class="contact100-more">
			
		</span>
	</div>



	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
