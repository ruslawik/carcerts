@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        <h3>Проверьте импортируемые данные для сертификата <b>"{{$cert_name}}"</b></h3>
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.people.index') }}">
                    Вернуться назад
                </a>
            </div>
            <form action="/admin/load-import" method="POST">
            @csrf
            <table class="table table-bordered table-striped">
                    <input type="hidden" name="cert_id" value="{{$cert_id}}">
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($rows[0] as $row)
                        <tr>
                            <input type="hidden" name="people_name[]" value="{{$row[0]}}">
                            <input type="hidden" name="people_email[]" value="{{$row[1]}}">
                            <td>{{$i}}</td>
                            <td>{{$row[0]}}</td>
                            <td>{{$row[1]}}</td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>
                    <input type="hidden" name="all_people" value="{{$i}}">
            </table>
            <button type="submit" class="btn btn-success" style="float:right;">Все верно, загрузить в базу</button>
            </form>
        </div>
    </div>
</div>



@endsection