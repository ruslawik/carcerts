@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.person.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.people.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="email">{{ trans('cruds.person.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email') }}" required>
                @if($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.person.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.person.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.person.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="certificate_id">{{ trans('cruds.person.fields.certificate') }}</label>
                <select class="form-control select2 {{ $errors->has('certificate') ? 'is-invalid' : '' }}" name="certificate_id" id="certificate_id" required>
                    @foreach($certificates as $id => $entry)
                        <option value="{{ $id }}" {{ old('certificate_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('certificate'))
                    <span class="text-danger">{{ $errors->first('certificate') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.person.fields.certificate_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="cert_num">{{ trans('cruds.person.fields.cert_num') }}</label>
                <input class="form-control {{ $errors->has('cert_num') ? 'is-invalid' : '' }}" type="text" name="cert_num" id="cert_num" value="{{ old('cert_num', '') }}">
                @if($errors->has('cert_num'))
                    <span class="text-danger">{{ $errors->first('cert_num') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.person.fields.cert_num_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="bergen_date">{{ trans('cruds.person.fields.bergen_date') }}</label>
                <input class="form-control datetime {{ $errors->has('bergen_date') ? 'is-invalid' : '' }}" type="text" name="bergen_date" id="bergen_date" value="{{ old('bergen_date') }}">
                @if($errors->has('bergen_date'))
                    <span class="text-danger">{{ $errors->first('bergen_date') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.person.fields.bergen_date_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection