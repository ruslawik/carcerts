@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.item.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.items.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="cert_id">{{ trans('cruds.item.fields.cert') }}</label>
                <select class="form-control select2 {{ $errors->has('cert') ? 'is-invalid' : '' }}" name="cert_id" id="cert_id" required>
                    @foreach($certs as $id => $entry)
                        <option value="{{ $id }}" {{ old('cert_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('cert'))
                    <span class="text-danger">{{ $errors->first('cert') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.item.fields.cert_helper') }}</span>
            </div>
            <div class="form-group" id="page_id_ajax_div">
                <label class="required" for="page_id">{{ trans('cruds.item.fields.page') }}</label>
                <select class="select2 form-control {{ $errors->has('page') ? 'is-invalid' : '' }}" name="page_id" id="page_id" required>

                </select>
                @if($errors->has('page'))
                    <span class="text-danger">{{ $errors->first('page') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.item.fields.page_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.item.fields.type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Item::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', 'name') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <span class="text-danger">{{ $errors->first('type') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.item.fields.type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="qr_size">{{ trans('cruds.item.fields.qr_size') }}</label>
                <input class="form-control {{ $errors->has('qr_size') ? 'is-invalid' : '' }}" type="number" name="qr_size" id="qr_size" value="{{ old('qr_size', '50') }}" step="1">
                @if($errors->has('qr_size'))
                    <span class="text-danger">{{ $errors->first('qr_size') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="x_axis">{{ trans('cruds.item.fields.x_axis') }}</label>
                <input class="form-control {{ $errors->has('x_axis') ? 'is-invalid' : '' }}" type="number" name="x_axis" id="x_axis" value="{{ old('x_axis', '0') }}" step="1">
                @if($errors->has('x_axis'))
                    <span class="text-danger">{{ $errors->first('x_axis') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.item.fields.x_axis_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="y_axis">{{ trans('cruds.item.fields.y_axis') }}</label>
                <input class="form-control {{ $errors->has('y_axis') ? 'is-invalid' : '' }}" type="number" name="y_axis" id="y_axis" value="{{ old('y_axis', '0') }}" step="1">
                @if($errors->has('y_axis'))
                    <span class="text-danger">{{ $errors->first('y_axis') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.item.fields.y_axis_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="html_color">{{ trans('cruds.item.fields.html_color') }}</label>
                <input class="form-control {{ $errors->has('html_color') ? 'is-invalid' : '' }}" type="text" name="html_color" id="html_color" value="{{ old('html_color', '#ffffff') }}">
                @if($errors->has('html_color'))
                    <span class="text-danger">{{ $errors->first('html_color') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="font_size">{{ trans('cruds.item.fields.font_size') }}</label>
                <input class="form-control {{ $errors->has('font_size') ? 'is-invalid' : '' }}" type="number" name="font_size" id="font_size" value="{{ old('font_size', '18') }}" step="1">
                @if($errors->has('font_size'))
                    <span class="text-danger">{{ $errors->first('font_size') }}</span>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $( document ).ready(function() {
            $('#cert_id').on('select2:select', function (e) {
                $('#page_id').select2({
                  ajax: {
                    url: '/admin/items-get-select-pages-ajax',
                    data: function (params) {
                        var query = {
                            certid: e.params.data.id,
                        }
                        return query;
                    },
                    dataType: 'json',
                    type: "GET",
                    processResults: function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: "Страница "+item.text,
                                    id: item.id
                                }
                            })
                        };
                    }
                  }
                });
            });
        });
    </script>
@endsection